<?php
/**
 * @package Affiliate Conversions
 */
/*
/*
Plugin Name: Affiliate Conversions
Plugin URI: https://devontrae.com/plugins/affiliate-conversions
Description: An extension which integrates affiliates plugin to track conversions using the WP HTTP API
Version: 1.0.0
Author: Devontrae
Author URI: https://devontrae.com/bio
License: GPLv2 or later
Text Domain: conversions
*/

if ( ! function_exists('add_action')) { 
    die('fuck off.');
}

class ConversionsPlugin {
    function __construct() {
    }

    function register() {
        add_action( 'rest_api_init', array($this, 'custom_endpoint'));
    }

    public function custom_endpoint() {
        register_rest_route( 'affiliates/v1', '/referral', [
            'methods' => 'POST',
            'callback' => [$this, 'new_referral'],
        ]);
    }

    public function new_referral( WP_REST_Request $request ) {
        global $wpdb;
      
        $return = new stdClass();
       
        if ($request->get_query_params()['token'] === JWT_AUTH_SECRET_KEY) {
            $param = $request->get_json_params();
            $query = $wpdb->insert($wpdb->base_prefix . 'aff_referrals', array(
                'affiliate_id' => $param['affiliate_id'], 
                'post_id' => '0', 
                'datetime' => new DateTime(), 
                'description' => $param['description'], 
                'user_id' => $param['user_id'], 
                'amount' => $param['commission'], 
                'reference_amount' => $param['revenue'], 
                'currency_id' => 'USD', 
                'status' => 'accepted', 
                'type' => 'manual', 
                'reference' => $param['reference'],
                'ip' => $param['ip']
            ));
            if ($query) {
                $return->success = true;
            } else {
              $return->success = false;
              $return->error = "BAD QUERY";
            }
        } else {
            $return->success = false;
            $return->error = "AUTH KEY REQUIRED";

            $return->key = JWT_AUTH_SECRET_KEY;
        }
        return $return;
    }
    function activate() {
        // generate CPT
        // flush rewrites
        flush_rewrite_rules();
    }

   function deactivate() {
    // flush rewrite rules
    flush_rewrite_rules();
   }
}

if (class_exists('ConversionsPlugin')) {
    $conversionsPlugin = new conversionsPlugin();
    $conversionsPlugin->register();
} else {
    die('Plugin not defined.');
}

# Register Activation Hook
register_activation_hook(__FILE__, array( $conversionsPlugin, 'activate'));

# Register Deactivation Hook
register_deactivation_hook(__FILE__, array( $conversionsPlugin, 'deactivate'));


